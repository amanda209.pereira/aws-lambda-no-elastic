//Create a client instance
var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
  host: 'https://search-es-lambda-domain-6oypkzkj6tvl3izpkz7yxy5o5a.us-west-2.es.amazonaws.com',
  log: 'trace',
  apiVersion: '7.4', // use the same version of your Elasticsearch instance
});

function ping() {
  client.ping({
    requestTimeout: 30000
  }, function (error) {
    if (error) {
      console.trace('Error:', error);
    } else {
      console.log('Connected!');
    }
    // on finish
    client.close();
  });
}

function deleteIndex() {
  client.indices.delete({ index: 'es-lambda-index' }, (err, res, status) => {
    if (res) {
      console.log('Index Have Been Deleted!');
    } else {
      console.log(err, res, status);
    }
  });
}

function createIndex() {
  client.indices.exists({ index: 'es-lambda-index' }, (err, res, status) => {
    if (res) {
      console.log('index es-lambda-index already exists!');
    } else {
      client.indices.create({
        index: 'es-lambda-index'
      }, (err, res, status) => {
        console.log(err, res, status);
      });
    }
  });
}

function putMapping() {
  console.log("Creating Mapping Index");

  client.indices.putMapping({
    index: 'es-lambda-index',
    body: {
      properties: {
        Id:           { type: 'keyword' },
        InsertDate:   { type: 'date' },
        Description:  { type: 'text' },
        Status:       { type: 'boolean' }
      }
    }
  }, (err, resp, status) => {
    if (err) {
      console.error(err, status);
    }
    else {
      console.log('successfully created mapping es-lambda-type', status, resp);
    }
  });
}

/* Call functions
ping();

deleteIndex();
createIndex();
putMapping();
*/

putMapping();
