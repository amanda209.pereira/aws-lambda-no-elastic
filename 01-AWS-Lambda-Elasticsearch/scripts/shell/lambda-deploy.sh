dotnet lambda deploy-function \
    --function-name LambdaInsertElasticSearch \
    --region us-west-2 \
    --profile default \
    --profile-location ./credentials/credentials.ini \
    --configuration Release \
    --framework netcoreapp3.1 \
    --function-runtime dotnetcore3.1 \
    --function-memory-size 256 \
    --function-timeout 30 \
    --function-handler LambdaES::Lambda.LambdaInsertElasticSearch::FunctionHandlerAsync
