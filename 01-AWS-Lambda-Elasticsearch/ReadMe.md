
I. Prepare Local Environment:

https://www.debian.org/releases/buster/debian-installer/

sudo apt install lsb-release
lsb_release -da

sudo apt install nodejs npm
node -v && npm -v

https://code.visualstudio.com/download

code --version

https://docs.microsoft.com/pt-br/dotnet/core/install/linux-package-manager-debian10

dotnet --info
dotnet --list-sdks
dotnet --list-runtimes

https://aws.amazon.com/sdk-for-net/
https://docs.aws.amazon.com/lambda/latest/dg/csharp-package-cli.html

dotnet new -all
dotnet new -i Amazon.Lambda.Templates
dotnet new lambda --list

dotnet tool install -g Amazon.Lambda.Tools
dotnet tool update -g Amazon.Lambda.Tools

https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html

aws --version
aws help
aws configure

https://www.npmjs.com/package/elasticsearch

sudo npm install -g jshint

npm init
npm install elasticsearch


II. Prepare Remote Environment:

https://aws.amazon.com/premiumsupport/knowledge-center/create-and-activate-aws-account/
https://aws.amazon.com/premiumsupport/knowledge-center/what-is-free-tier/

create and activate Aws account

https://aws.amazon.com/elasticsearch-service/getting-started/

start Aws Elasticsearch Service

https://aws.amazon.com/ec2/instance-types/
https://www.elastic.co/guide/en/elasticsearch/plugins/master/cloud-aws-best-practices.html

choice Instance Type and Storage
IP-based security


III. Code Development:

dotnet new lambda.EmptyFunction --name LambdaInsertElasticSearch --profile default --region us-west-2

add item group in LambdaInsertElasticSearch.csproj to include credentials/credentials.ini

https://www.nuget.org/packages/Elasticsearch.Net/
https://www.nuget.org/packages/Elasticsearch.Net.Aws/
https://www.nuget.org/packages/NEST/

dotnet add package Elasticsearch.Net --version 7.7.0
dotnet add package Elasticsearch.Net.Aws --version 7.0.4
dotnet add package NEST --version 7.7.0

add the design classes in the base architecture

run UnitTest with Xunit

https://{DOMAIN}/_plugin/kibana/

GET _cat/indices?pretty

GET /es-lambda-index/_settings?pretty

GET /es-lambda-index/_mapping?pretty

GET es-lambda-index/_search
{
  "query": {
    "match_all": {}
  }
}


IV. Deploy Lambda:

https://www.nuget.org/packages/Amazon.Lambda.Tools

dotnet add package Amazon.Lambda.Tools --version 4.0.0

https://docs.microsoft.com/en-us/dotnet/core/tools/troubleshoot-usage-issues

nano $HOME/.bashrc

export PATH="$PATH:$HOME/.dotnet/tools"
source .bashrc

read LambdaInsertElasticSearch/src/LambdaInsertElasticSearch/Readme.md

dotnet lambda --help
dotnet lambda deploy-function LambdaInsertElasticSearch -f netcoreapp2.1


V. Informations and Conclusions:

https://aws.amazon.com/pt/about-aws/global-infrastructure/regional-product-services/
https://docs.aws.amazon.com/elasticsearch-service/latest/developerguide/aes-bp.html


https://aws.amazon.com/blogs/compute/announcing-aws-lambda-supports-for-net-core-3-1/
