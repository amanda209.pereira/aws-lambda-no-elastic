using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace LambdaCoreElasticUtils
{
    public static class StringHelper
    {
        public static string ToPascalCase(this string text)
        {
            TextInfo info = Thread.CurrentThread.CurrentCulture.TextInfo;
            text = info.ToTitleCase(text);

            string[] parts =
                text.Split(
                    new char[] { },
                    StringSplitOptions.RemoveEmptyEntries);

            return String.Join(String.Empty, parts);
        }

        public static string ToCamelCase(this string text)
        {
            return text.Substring(0, 1).ToLower() + text.Substring(1);
        }

        public static string ToTitleCase(this string text)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(text);
        }

        // Capitalize the first character and add a space before
        // each capitalized letter (except the first character).
        public static string ToProperCase(this string text)
        {
            const string pattern = @"(?<=\w)(?=[A-Z])";
            //const string pattern = @"(?<=[^A-Z])(?=[A-Z])";

            string result = Regex.Replace(text, pattern, "", RegexOptions.None);

            return result.Substring(0, 1).ToUpper() + result.Substring(1);
        }

        public static string RemoveDiacritics(this string text)
        {
            return string.Concat(
                text.Normalize(NormalizationForm.FormD)
                    .Where(ch => CharUnicodeInfo.GetUnicodeCategory(ch) != UnicodeCategory.NonSpacingMark))
                    .Normalize(NormalizationForm.FormC);
        }

        public static string Base64Encode(this string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(this string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}