using System;
using System.IO;
using System.Reflection;

namespace LambdaCoreElasticUtils
{
    public class AssemblyHelper
    {
        public static string DirectoryName
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);

                return Path.GetDirectoryName(path);
            }
        }
    }
}