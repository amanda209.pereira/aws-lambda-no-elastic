using System.IO;

using Amazon.Runtime;
using Amazon.Runtime.CredentialManagement;

using LambdaCoreElasticUtils;

namespace LambdaCoreActions
{
    public abstract class BaseAction
    {
        protected AWSCredentials AWSCredentials = null;

        public BaseAction()
        {
            var directoryName = AssemblyHelper.DirectoryName;

            var storeChain = new CredentialProfileStoreChain(Path.Combine(directoryName, "credentials.ini"));
            
            if (!storeChain.TryGetAWSCredentials("default", out this.AWSCredentials))
            {
                throw new AmazonClientException(nameof(storeChain.TryGetAWSCredentials));
            }
        }
    }
}