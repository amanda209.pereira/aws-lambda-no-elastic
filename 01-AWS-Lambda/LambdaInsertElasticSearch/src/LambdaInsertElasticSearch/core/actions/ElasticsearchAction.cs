using System;
using System.Threading;
using System.Threading.Tasks;

using Amazon;

using LambdaCoreServices;

namespace LambdaCoreActions
{
    public class ElasticSearchAction : BaseAction, IDisposable
    {
        private ElasticSearchService elasticsearchService = null;

        public ElasticSearchAction(string serviceUrl, string region)
        {
            this.elasticsearchService =
                new ElasticSearchService(
                    base.AWSCredentials,
                    RegionEndpoint.GetBySystemName(region),
                    serviceUrl
                );
        }
        public void Dispose()
        {
            this.elasticsearchService.Dispose();
        }

        public async Task<bool> SaveAsync<T>(T entity)
            where T: class
        {
            return await this.elasticsearchService.SaveAsync<T>(entity);
        }

        public async Task<bool> SaveAsync<T>(T entity, string index, dynamic id)
            where T: class
        {
            return await this.elasticsearchService.SaveAsync<T>(entity, index, id, default(CancellationToken));
        }
    }
}