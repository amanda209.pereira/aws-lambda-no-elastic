using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime;
using Elasticsearch.Net;
using Elasticsearch.Net.Aws;
using Nest;

using LambdaCoreElasticUtils;

namespace LambdaCoreServices
{
    public class ElasticSearchService : IDisposable
    {
        private ElasticClient ElasticClient = null;

        public ElasticSearchService(AWSCredentials awsCredentials, RegionEndpoint regionEndpoint, string serviceUrl)
        {
            var credentials = awsCredentials.GetCredentials();

            var httpConnection = 
                new AwsHttpConnection(
                    new BasicAWSCredentials(credentials.AccessKey, credentials.SecretKey),
                    regionEndpoint
                );
            
            var settings = 
                new ConnectionSettings(
                    new SingleNodeConnectionPool(new Uri(serviceUrl)),
                    httpConnection
                );

            this.ElasticClient = new ElasticClient(settings);
        }

        public void Dispose()
        {
        }

        public async Task<T> GetAsync<T>(T entity) 
            where T : class
        {
            try
            {
                var type = typeof(T).Name.ToLower();

                var response = 
                    await this.ElasticClient.GetAsync<T>(
                        new DocumentPath<T>(entity),
                        g => g.Index(type)
                    );
 
                if (response.IsValid)
                {
                    return response.Source;
                }
            }
            catch (ElasticsearchClientException e)  { throw e; }
            catch (AmazonServiceException e)        { throw e; }
            catch (Exception e)                     { throw e; }

            return default(T);
        }

        public async Task<bool> SaveAsync<T>(T entity) 
            where T : class
        {
            try
            {
                var type    = GetAttributeName<T>() ?? typeof(T).Name.ToLower();
                dynamic id  = GetKeyPropertyValue<T>(entity);                
                
                return await this.SaveAsync<T>(entity, type, id, default(CancellationToken));
            }
            catch (ElasticsearchClientException e)  { throw e; }
            catch (AmazonServiceException e)        { throw e; }
            catch (Exception e)                     { throw e; }
        }

        public async Task<bool> SaveAsync<T>(T entity, string index, dynamic id, CancellationToken ct) 
            where T : class
        {
            try
            {   
                var result = 
                    await this.ElasticClient.IndexAsync<T>(
                        entity, 
                        i => i
                            .Index(index)
                            .Id(id)
                            .Refresh(Refresh.False),
                        ct);

                if (!result.IsValid)
                {
                    throw result.OriginalException;
                }

                return result.IsValid;
            }
            catch (ElasticsearchClientException e)  { throw e; }
            catch (AmazonServiceException e)        { throw e; }
            catch (Exception e)                     { throw e; }
        }
        
        #region Private Method(s)

        private object GetKeyPropertyValue<T>(T entity)
        {
            var attributes = typeof(T).GetCustomAttributes(false);

            foreach (dynamic attribute in attributes)
            {
                if (attribute.GetType().Equals(typeof(ElasticsearchTypeAttribute)))
                {
                    return typeof(T).GetProperty(attribute.IdProperty).GetValue(entity, null);
                }
            }

            return null;
        }

        private string GetAttributeName<T>()
        {
            var attributes = typeof(T).GetCustomAttributes(false);

            foreach (dynamic attribute in attributes)
            {
                if (attribute.GetType().Equals(typeof(ElasticsearchTypeAttribute)))
                {
                    return attribute.Name;
                }
            }
            
            return null;
        }
           
        private string GetAttributeName<T>(string property)
        {
            var propertyInfo = typeof(T).GetProperty(property);

            if (propertyInfo != null)
            {
                var attributes = propertyInfo.GetCustomAttributes(false);

                foreach (dynamic attribute in attributes)
                {
                    if (attribute.GetType().Equals(typeof(TextAttribute)))
                    {
                        return attribute.Name;
                    }
                }
            }
            
            return null;
        }

        #endregion
    }
}