using System;
using System.Net;

using Amazon.Lambda.Core;

using LambdaCoreActions;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
//[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace LambdaInsertElasticSearch
{
    public class Function
    {        
        /// <summary>
        /// A simple function that insert object in Elasticsearch
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public string FunctionHandler(ILambdaContext context)
        {            
            using (ElasticSearchAction elasticSearchAction = 
                new ElasticSearchAction(
                    "https://search-es-lambda-domain-6oypkzkj6tvl3izpkz7yxy5o5a.us-west-2.es.amazonaws.com",
                    "us-west-2"
                )
            )
            {
                dynamic dataObject = 
                    new 
                    {
                        Id          = Guid.NewGuid(),
                        InsertDate  = DateTime.UtcNow,
                        Description = nameof(elasticSearchAction.SaveAsync),
                        Status      = false
                    };

                bool saveOk = 
                    elasticSearchAction.SaveAsync<dynamic>(
                        dataObject, "es-lambda-index", dataObject.Id
                    ).Result;

                if (saveOk == default(bool))
                {
                    throw new ApplicationException(
                        string.Format("{0}:{1}", 
                            nameof(elasticSearchAction.SaveAsync), 
                            dataObject.Id
                        )
                    );
                }
            }

            return HttpStatusCode.OK.ToString();
        }
    }
}
