
using Xunit;
using Amazon.Lambda.TestUtilities;

namespace LambdaInsertElasticSearch.Tests
{
    public class FunctionTest
    {
        [Fact]
        public void TestToInsertElasticSearch()
        {
            var function = new Function();
            var context = new TestLambdaContext();
            var result = function.FunctionHandler(context);

            Assert.Equal("OK", result);
        }
    }
}
