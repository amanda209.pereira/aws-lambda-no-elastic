cd ../../LambdaInsertElasticSearch/src/LambdaInsertElasticSearch

dotnet lambda deploy-function \
    --function-name LambdaInsertElasticSearch \
    --region us-west-2 \
    --profile default \
    --profile-location ./credentials/credentials.ini \
    --configuration Release \
    --framework netcoreapp2.1 \
    --function-runtime dotnetcore2.1 \
    --function-memory-size 256 \
    --function-timeout 30 \
    --function-handler LambdaInsertElasticSearch::LambdaInsertElasticSearch.Function::FunctionHandler
